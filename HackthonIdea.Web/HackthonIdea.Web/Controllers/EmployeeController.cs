﻿using HackthonIdea.Web.Class;
using HackthonIdea.Web.Models;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using System;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace HackthonIdea.Web.Controllers
{
    public class EmployeeController : Controller
    {
        private readonly string baseUrl = "https://localhost:44345/api/RegisterEmployee/AddEmployee";
        public IActionResult Register()
        {
            return View();
        }
        [HttpPost]
        public async Task<IActionResult> RegisterAsync(EmployeeModel employeeModel)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    using (var httpClient = new HttpClient())
                    {
                        StringContent employee = new StringContent(JsonConvert.SerializeObject(employeeModel), Encoding.UTF8, "application/json");
                        using (var response = await httpClient.PostAsync("https://localhost:44345/api/RegisterEmployee/AddEmployee", employee))
                        {
                            string apiResponse = await response.Content.ReadAsStringAsync();
                            var res = JsonConvert.DeserializeObject<EmployeeModel>(apiResponse);
                        }
                    }
                    return RedirectToAction("Register", "Employee");
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
            else
            {
                return View(employeeModel);
            }
        }

        public IActionResult LogIn()
        {
            ViewBag.IsInvalid = false;
            return View();
        }
        [HttpPost]
        public async Task<IActionResult> LogIn(LoginModel login)
        {
            ViewBag.IsInvalid = false;
            if (ModelState.IsValid)
            {
                try
                {
                    using (var httpClient = new HttpClient())
                    {
                        string encryptedValues = Convert.ToBase64String(Encoding.UTF8.GetBytes($"{login.Username}:{login.Password}"));
                        httpClient.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Basic", encryptedValues);
                        using (var response = await httpClient.GetAsync("https://localhost:44345/api/User/GetUser"))
                        {
                            string apiResponse = await response.Content.ReadAsStringAsync();
                            EmployeeModel employee = JsonConvert.DeserializeObject<EmployeeModel>(apiResponse);
                            if (response.StatusCode.ToString() == "OK" && employee != null)
                            {
                                APIHelpers.Initialize(employee.EmpId, employee.FirstName, employee.Email,employee.UserId, employee.Password);
                                return RedirectToAction("Index", "Home");
                            }
                            else
                            {
                                ViewBag.IsInvalid = true;
                            }
                        }
                    }
                    return View();
                }
                catch (Exception ex)
                {
                    return null;
                }
            }
            else
            {
                return View(login);
            }

        }
    }
}
