﻿using HackthonIdea.Web.Class;
using HackthonIdea.Web.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace HackthonIdea.Web.Controllers
{
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;
        private readonly string baseUrl = "https://localhost:44345/api/HackIdea/";

        public HomeController(ILogger<HomeController> logger)
        {
            _logger = logger;
        }

        public async Task<IActionResult> Index(string sortOrder, string searchString)
        {
            try
            {
                List<HackIdeaModel> ideaList = new List<HackIdeaModel>();
                ViewData["UpVoteSortParm"] = String.IsNullOrEmpty(sortOrder) ? "upvote_desc" : "";
                ViewData["DateSortParm"] = sortOrder == "Date" ? "date_desc" : "Date";
                ViewData["CurrentFilter"] = searchString;

                using (var httpClient = new HttpClient())
                {
                    string encryptedValues = GetEncryptedCred();
                    httpClient.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Basic", encryptedValues);
                    using (var response = await httpClient.GetAsync(baseUrl + "GetAllIdea"))
                    {
                        string apiResponse = await response.Content.ReadAsStringAsync();
                        ideaList = JsonConvert.DeserializeObject<List<HackIdeaModel>>(apiResponse);
                    }

                }
                if (!String.IsNullOrEmpty(searchString))
                {
                    ideaList = ideaList.Where(s => s.Title.Contains(searchString)
                                           || s.Description.Contains(searchString)).ToList();
                }
                switch (sortOrder)
                {
                    case "upvote_desc":
                        ideaList = ideaList.OrderByDescending(s => s.UpVote).ToList();
                        break;
                    case "Date":
                        ideaList = ideaList.OrderBy(s => s.CreationDate).ToList();
                        break;
                    case "date_desc":
                        ideaList = ideaList.OrderByDescending(s => s.CreationDate).ToList();
                        break;
                    default:
                        ideaList = ideaList.OrderBy(s => s.UpVote).ToList();
                        break;
                }

                return View(ideaList);
            }
           catch(Exception)
            {
                //APIHelpers.Initialize(0,null,null,null,null);
                return RedirectToAction("LogIn", "Employee");
            }
        }

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }

        public async Task<IActionResult> AddOrEdit(int? ideaId)
        {
            ViewBag.PageName = ideaId == null ? "Create Idea" : "Edit Idea";
            ViewBag.IsEdit = ideaId == null ? false : true;
            List<HackTagModel> tagList = new List<HackTagModel>();
            using (var httpClient = new HttpClient())
            {
                string encryptedValues = GetEncryptedCred();
                httpClient.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Basic", encryptedValues);
                using (var response = await httpClient.GetAsync(baseUrl + "GetAllTag"))
                {
                    string apiResponse = await response.Content.ReadAsStringAsync();
                    tagList = JsonConvert.DeserializeObject<List<HackTagModel>>(apiResponse);
                }
            }
            ViewBag.tags = tagList;
            if (ideaId == null)
            {
                return View();
            }
            else
            {
                HackIdeaModel hackIdeaModel = new HackIdeaModel();
                using (var httpClient = new HttpClient())
                {
                    string encryptedValues = GetEncryptedCred();
                    httpClient.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Basic", encryptedValues);
                    using (var response = await httpClient.GetAsync(baseUrl + "GetIdeaById?id="+ideaId))
                    {
                        string apiResponse = await response.Content.ReadAsStringAsync();
                        hackIdeaModel = JsonConvert.DeserializeObject<HackIdeaModel>(apiResponse);
                    }
                }
                return View(hackIdeaModel);
            }
        }
        [HttpPost]
        public async Task<IActionResult> AddOrEdit(HackIdeaModel ideaModel)
        {
            
            if (ModelState.IsValid)
            {
                try
                {
                    ideaModel.EmpId = APIHelpers.EmployeeId;

                    using (var httpClient = new HttpClient())
                    {
                        string encryptedValues = GetEncryptedCred();
                        httpClient.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Basic", encryptedValues);
                        StringContent hackIdea = new StringContent(JsonConvert.SerializeObject(ideaModel), Encoding.UTF8, "application/json");
                        using (var response = await httpClient.PostAsync(baseUrl + "AddIdea", hackIdea))
                        {
                            string apiResponse = await response.Content.ReadAsStringAsync();
                            //JsonConvert.DeserializeObject<HackIdeaModel>(apiResponse);
                        }
                    }
                    return RedirectToAction("Index");
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
            else
            {
                List<HackTagModel> tagList = new List<HackTagModel>();
                using (var httpClient = new HttpClient())
                {
                    string encryptedValues = GetEncryptedCred();
                    httpClient.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Basic", encryptedValues);
                    using (var response = await httpClient.GetAsync(baseUrl + "GetAllTag"))
                    {
                        string apiResponse = await response.Content.ReadAsStringAsync();
                        tagList = JsonConvert.DeserializeObject<List<HackTagModel>>(apiResponse);
                    }
                }
                ViewBag.tags = tagList;
                ViewBag.PageName = ideaModel.IdeaId <=0 ? "Create Idea" : "Edit Idea";
                ViewBag.IsEdit = ideaModel.IdeaId == 0 ? false : true;
                return View(ideaModel);
            }
            
        }
        public async Task<IActionResult> DeleteIdea(int ideaId)
        {
            try
            {
                using (var httpClient = new HttpClient())
                {
                    string encryptedValues = GetEncryptedCred();
                    httpClient.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Basic", encryptedValues);
                    using (var response = await httpClient.DeleteAsync(baseUrl + "DeleteIdea?ideaId=" + ideaId))
                    {
                        string apiResponse = await response.Content.ReadAsStringAsync();
                        int res = JsonConvert.DeserializeObject<int>(apiResponse);
                    }
                }
            }
            catch(Exception ex)
            {
                throw ex;
            }
           
            return RedirectToAction("Index");
        }

        public async Task<IActionResult> Upvote(int ideaId)
        {
            try
            {
                HackIdeaModel hackIdeaModel = new HackIdeaModel();
                using (var httpClient = new HttpClient())
                {
                    string encryptedValues = GetEncryptedCred();
                    httpClient.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Basic", encryptedValues);
                    //StringContent hackIdeaModel = new StringContent(JsonConvert.SerializeObject(ideaModel), Encoding.UTF8, "application/json");
                    using (var response = await httpClient.GetAsync(baseUrl + "GetIdeaById?id=" + ideaId))
                    {
                         string apiResponse = await response.Content.ReadAsStringAsync();
                          hackIdeaModel = JsonConvert.DeserializeObject<HackIdeaModel>(apiResponse);
                    }
                    
                }
                using (var httpClient = new HttpClient())
                {
                    string encryptedValues = GetEncryptedCred();
                    httpClient.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Basic", encryptedValues);
                    StringContent ideaModel = new StringContent(JsonConvert.SerializeObject(hackIdeaModel), Encoding.UTF8, "application/json");
                    //StringContent ideaModel = new StringContent(JsonConvert.SerializeObject(hackIdeaModel), Encoding.UTF8, "application/json");
                    using (var response = await httpClient.PutAsync(baseUrl + "Upvote", ideaModel))
                    {
                        string apiResponse = await response.Content.ReadAsStringAsync();
                        //hackIdeaModel = JsonConvert.DeserializeObject<HackIdeaModel>(apiResponse);
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return RedirectToAction("Index");
        }
        public string GetEncryptedCred()
        {
            string encryptedValues = Convert.ToBase64String(Encoding.UTF8.GetBytes($"{APIHelpers.Username}:{APIHelpers.Password}"));
            return encryptedValues;
            //httpClient.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Basic", encryptedValues);
        }
    }
    
}
