﻿using System;
using System.ComponentModel.DataAnnotations;

namespace HackthonIdea.Web.Models
{
    public class HackIdeaModel
    {
        public int IdeaId { get; set; }
        public int EmpId { get; set; }
        [Required]
        public string Title { get; set; }
        [Required]
        public string Description { get; set; }
        [Required]
        public int? TagId { get; set; }
        public int? UpVote { get; set; }
        public bool? IsActive { get; set; }
        public DateTime? CreationDate { get; set; }
        public DateTime? MadifiedDate { get; set; }
        public string TagName { get; set; }
    }
}
