﻿namespace HackthonIdea.Web.Models
{
    public class HackTagModel
    {
        public int TagId { get; set; }
        public string TagName { get; set; }
        public string TagdDesc { get; set; }
    }
}
