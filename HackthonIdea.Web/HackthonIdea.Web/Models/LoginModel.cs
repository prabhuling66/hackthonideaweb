﻿using System.ComponentModel.DataAnnotations;

namespace HackthonIdea.Web.Models
{
    public class LoginModel
    {
        [Required(ErrorMessage ="Please enter username")]
        public string Username { get; set; }
        [Required(ErrorMessage ="Please enter password")]
        public string Password { get; set; }
    }
}
