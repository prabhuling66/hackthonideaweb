﻿using System.ComponentModel.DataAnnotations;

namespace HackthonIdea.Web.Models
{
    public class EmployeeModel
    {
        public int EmpId { get; set; }
        [Required(ErrorMessage ="Fisrt Name Required")]
        public string FirstName { get; set; }
        [Required(ErrorMessage = "Last Name Required")]
        public string LastName { get; set; }
        [Required(ErrorMessage = "User Name Required")]
        public string UserId { get; set; }
        [Required(ErrorMessage = "Password Required")]
        public string Password { get; set; }
        [Required(ErrorMessage = "Email Required")]
        public string Email { get; set; }
        [Required(ErrorMessage = "Contact Number Required")]
        public string Mobile { get; set; }
        public bool? IsActive { get; set; }
    }
}
